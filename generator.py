"""
PUZZLE GENERATOR
"""

import json

from sudoku import Grid
from crook import solve

from itertools import combinations
from random import choice, shuffle, sample

def generate(clues):
    
    def solve(puz):
        
        grid = Grid(puz)
        
        psets = [{cell for cell in rgn if not cell.num} for rgn in grid.rows + grid.cols + grid.boxes]
        while psets:
            
            pset_found = False
            for pset in psets:
                for pset_len in range(1, max(len(pset), 2)):
                    ppset_cells = {cell for cell in pset if len(cell.markup) <= pset_len} #ppset = possible preemptive set - pset without cells with too many markup nums for this pset_len, in order to eliminate obviously unsuccessful combinations
                    for pset_cells in combinations(ppset_cells, pset_len):
                        pset_markup = set()
                        for cell in pset_cells:
                            pset_markup |= cell.markup
                        pset_found = len(pset_markup) == pset_len
                        if pset_found:
                            psubset = set(pset_cells)
                            psuperset_pos = psets.index(pset)
                            for pset_ in psets:
                                if pset_.issuperset(psubset):
                                    pset_ -= psubset
                                    for cell in pset_:
                                        cell.markup -= pset_markup
                                        if not cell.markup: #cell with no possible number due to violation
                                            return False
                            if pset_len != 1:
                                psets.insert(psuperset_pos, psubset)
                            else:
                                pset_cells[0].num = next(iter(pset_markup)) #gets singleton, only element in set
                                pset_cells[0].markup.clear()
            psets = [pset for pset in psets if pset] #Remove empty psets
            
            if not psets: #solved
                for cell in sample(grid.cells, 81 - clues):
                    cell.num = 0
                return "".join(str(num) for num in grid.get_puz())   
            
            if not pset_found: #requires search
                puz_copy = grid.get_puz()
                rand_cell = choice([cell for cell in grid.cells if not cell.num]) #random cell
                rand_cell_markup = list(rand_cell.markup)
                shuffle(rand_cell_markup)
                for num in rand_cell_markup: #random number in markup
                    puz_copy[rand_cell.cell_pos] = num
                    solution = solve(puz_copy)
                    if solution:
                        return solution
                
                return False #dead end
    
    return solve("0" * 81) #empty puzzle

puzzles = {}
for clues in range(1, 81):
    puzzles[str(clues).zfill(2)] = []
    for _ in range(100):
        puzzles[str(clues).zfill(2)].append(generate(clues))
with open("puzzles.json", "w") as f:
    json.dump(puzzles, f, indent=2, sort_keys=True)