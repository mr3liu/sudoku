"""
TIMER
"""

import json

from timeit import Timer

import brute, crook, knuth

with open("puzzles.json", "r") as f:
    puzzles = json.load(f)

results = {"brute": {}, "crook": {}, "knuth": {}}

for alg in ("brute", "crook", "knuth"):
    for clues in range(1, 81):
        clues = str(clues).zfill(2)
        results[alg][clues] = []
        stmt = alg + ".solve(puzzle)"
        timer = Timer(stmt, globals=globals())
        for puzzle in puzzles[clues]:
            if not eval(stmt):
                results[alg][clues].append("Unsolvable")
                continue
            repeat_result = []
            for _ in range(5): #required as repeat calls timeit instead of autorange
                timer_result = timer.autorange()
                repeat_result.append(timer_result[1] / timer_result[0])
            results[alg][clues].append(min(repeat_result))
 
with open("results.json", "w") as f:
    json.dump(results, f, indent=2, sort_keys=True)