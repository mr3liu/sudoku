'''
SUDOKU

>>> p = "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
>>> g = Grid(p)
>>> print(g.get_puz())
[0, 0, 3, 0, 2, 0, 6, 0, 0, 9, 0, 0, 3, 0, 5, 0, 0, 1, 0, 0, 1, 8, 0, 6, 4, 0, 0, 0, 0, 8, 1, 0, 2, 9, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 6, 7, 0, 8, 2, 0, 0, 0, 0, 2, 6, 0, 9, 5, 0, 0, 8, 0, 0, 2, 0, 3, 0, 0, 9, 0, 0, 5, 0, 1, 0, 3, 0, 0]
>>> g.print_puz()
003020600900305001001806400008102900700000008006708200002609500800203009005010300
>>> g.print_grid()
 +===========+===========+===========+ 
 I   |   | 3 I   | 2 |   I 6 |   |   I 
 I---+---+---I---+---+---I---+---+---I 
 I 9 |   |   I 3 |   | 5 I   |   | 1 I 
 I---+---+---I---+---+---I---+---+---I 
 I   |   | 1 I 8 |   | 6 I 4 |   |   I 
 +===========+===========+===========+ 
 I   |   | 8 I 1 |   | 2 I 9 |   |   I 
 I---+---+---I---+---+---I---+---+---I 
 I 7 |   |   I   |   |   I   |   | 8 I 
 I---+---+---I---+---+---I---+---+---I 
 I   |   | 6 I 7 |   | 8 I 2 |   |   I 
 +===========+===========+===========+ 
 I   |   | 2 I 6 |   | 9 I 5 |   |   I 
 I---+---+---I---+---+---I---+---+---I 
 I 8 |   |   I 2 |   | 3 I   |   | 9 I 
 I---+---+---I---+---+---I---+---+---I 
 I   |   | 5 I   | 1 |   I 3 |   |   I 
 +===========+===========+===========+ 
>>> g.print_grid(markup=True)
 +=========================================+=========================================+=========================================+ 
 I     {45}    |    {4578}   |      3      I     {49}    |      2      |    {147}    I      6      |    {5789}   |     {57}    I 
 I-------------+-------------+-------------I-------------+-------------+-------------I-------------+-------------+-------------I 
 I      9      |   {24678}   |     {47}    I      3      |     {47}    |      5      I     {78}    |    {278}    |      1      I 
 I-------------+-------------+-------------I-------------+-------------+-------------I-------------+-------------+-------------I 
 I     {25}    |    {257}    |      1      I      8      |     {79}    |      6      I      4      |   {23579}   |    {2357}   I 
 +=========================================+=========================================+=========================================+ 
 I    {345}    |    {345}    |      8      I      1      |    {3456}   |      2      I      9      |   {34567}   |   {34567}   I 
 I-------------+-------------+-------------I-------------+-------------+-------------I-------------+-------------+-------------I 
 I      7      |   {123459}  |     {49}    I    {459}    |   {34569}   |     {4}     I     {1}     |   {13456}   |      8      I 
 I-------------+-------------+-------------I-------------+-------------+-------------I-------------+-------------+-------------I 
 I    {1345}   |   {13459}   |      6      I      7      |    {3459}   |      8      I      2      |    {1345}   |    {345}    I 
 +=========================================+=========================================+=========================================+ 
 I    {134}    |    {1347}   |      2      I      6      |    {478}    |      9      I      5      |    {1478}   |     {47}    I 
 I-------------+-------------+-------------I-------------+-------------+-------------I-------------+-------------+-------------I 
 I      8      |    {1467}   |     {47}    I      2      |    {457}    |      3      I     {17}    |    {1467}   |      9      I 
 I-------------+-------------+-------------I-------------+-------------+-------------I-------------+-------------+-------------I 
 I     {46}    |    {4679}   |      5      I     {4}     |      1      |     {47}    I      3      |   {24678}   |    {2467}   I 
 +=========================================+=========================================+=========================================+ 
'''

class Cell:
    
    def __init__(self, num):
        self.num = num
        self.cell_pos = None
        self.row_pos = None
        self.col_pos = None
        self.box_pos = None 
        self.markup = set()
        self.neighbours = set()

class Grid:
    
    def __init__(self, puz):
        
        if type(puz) == str:
            puz = [int(num) for num in puz]
        self.cells = [Cell(num) for num in puz]
        for i, cell in enumerate(self.cells):
            cell.cell_pos = i
        
        self.rows = [self.cells[i * 9:(i + 1) * 9] for i in range(9)]
        for i, row in enumerate(self.rows):
            for cell in row:
                cell.row_pos = i
                
        self.cols = [[self.cells[i + j * 9] for j in range(9)] for i in range(9)]
        for i, col in enumerate(self.cols):
            for cell in col:
                cell.col_pos = i
                
        self.boxes = [[self.cells[i * 27 + j * 3 + k * 9 + l] for k in range(3) for l in range(3)] for i in range(3) for j in range(3)]
        for i, box in enumerate(self.boxes):
            for cell in box:
                cell.box_pos = i
        
        for curr_cell in self.cells:
            curr_cell.neighbours = set(self.rows[curr_cell.row_pos] + self.cols[curr_cell.col_pos] + self.boxes[curr_cell.box_pos])
            curr_cell.neighbours.remove(curr_cell)
        
        for curr_cell in self.cells:
            if not curr_cell.num:
                curr_cell.markup.update(range(1, 10))
                for cell in self.rows[curr_cell.row_pos] + self.cols[curr_cell.col_pos] + self.boxes[curr_cell.box_pos]:
                    if cell.num:
                        curr_cell.markup.discard(cell.num) 
        
    def get_puz(self):
        return [cell.num for cell in self.cells]
    
    def print_puz(self):
        print(*self.get_puz(), sep="")
    
    def print_grid(self, markup=False):
        if not markup:
            THIN_H_BORDER = " I---+---+---I---+---+---I---+---+---I \n"
            THICK_H_BORDER = " +===========+===========+===========+ \n"
        else:
            THIN_H_BORDER = " I-------------+-------------+-------------I-------------+-------------+-------------I-------------+-------------+-------------I \n"
            THICK_H_BORDER = " +=========================================+=========================================+=========================================+ \n"
        pp = ""
        pp += THICK_H_BORDER
        for i in range(9):
            pp += " I "
            for j in range(9):
                cell = self.rows[i][j]
                if not markup:
                    if cell.num:
                        pp += str(cell.num)
                    else:
                        pp += " " #more readable than 0
                else:
                    if cell.num:
                        pp += str(cell.num).center(11)
                    else:
                        pp += ("{" + "".join([str(num) for num in cell.markup]) + "}").center(11)
                if (j + 1) % 3 == 0:
                    pp += " I "
                else:
                    pp += " | "
            pp += "\n"
            if (i + 1) % 3 == 0:
                pp += THICK_H_BORDER
            else:
                pp += THIN_H_BORDER
        print(pp, end="")


if __name__ == "__main__":
    import doctest
    doctest.testmod()