# Sudoku

Text-based library for sudoku puzzle manipulation, generation, and algorithmic solving with efficiency testing.

`sudoku.py`: Puzzle objects

`brute.py`, `crook.py`, `knuth.py`: Solving algorithms

`generator.py`: Generates random puzzles, writes into `puzzles.json`

`timer.py`: Reads puzzles form `puzzles.json`, tests efficiency of algorithms with puzzles, writes into `results.json`
